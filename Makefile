sig2str.a: sig2str.o
	$(AR) crsu $@ $^

sig2str.o: sig2str.c sig2str.h signame.h

signame.h: signame.h.in generate-signame_h
	./generate-signame_h < signame.h.in > signame.h

clean:
	$(RM) sig2str.a sig2str.o signame.h generate-signame_h
