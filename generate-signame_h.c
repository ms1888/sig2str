#include <stdio.h>
#include <signal.h>

int main() {
	char *lineptr = NULL;
	size_t n = 0;
	ssize_t l;
	printf("#include <signal.h>\n\nstatic struct { int sig; const char *name; } signame[] = {\n\t{ 0, \"Signal 0\" },\n");
	while ((l = getline(&lineptr, &n, stdin)) > 0) {
		if (lineptr[l-1] == '\n')
			lineptr[l-1] = 0;
		printf("#ifdef SIG%s\n\t{ SIG%s, \"%s\" },\n#endif\n", lineptr, lineptr, lineptr);
	}
	for (int i = SIGRTMIN; i <= SIGRTMAX; i++)
		printf("\t{ %d, \"RT%d\" },\n", i, i-SIGRTMIN);
	printf("};");
}
